# Chapter 1: Defining the goals of Basis

Basis is a foundation for automation and democratization of economic-related processes (and the governance thereof) within the framework of a socialist mode of production. The ultimate goal is to replace as much governance as possible with automation and to enable self-governance by members where automation is infeasible.

Let's go over some of the key goals of Basis.

## Membership

Membership of the system is controlled by policies set by current members. In general, a member of the system is a person who works at a member company, and member companies generally can join (or be created) under the conditions that they:

- Are a worker-owned company or multi-stakeholder company where the workers have significant representation
- Use Basis for their transactional system and labor time tracking

Membership is not binary, but rather is a sliding scale decided by the number of hours worked at a member company. With more participation comes higher voting rights and more use of shared assets, such as housing.

Usage of the system is open to any non-member company (as a platform to sell their products and services for money/profit) for a monthly fee or per-transaction fee.

## Regions and locality

While Basis aims to be a federated network of groups of producers all working together to meet each others' needs, there still needs to be some level of local control. Nobody wants someone 1000 miles away telling them how to do things.

Communal assets would fall under regional control. The idea is that someone in New York shouldn't have any say over what happens to a tractor in Minnesota. Regions are groups of assets and economic parameters that are controlled by the members in that region.

## Cost tracking

Basis tracks disaggregate costs of products and services in terms of labor and resources. What does this mean? It means that a chair no longer costs $30, but rather 1.8 hours of labor, 12 kg wood, 3g steel, and 0.4l diesel fuel.

Why do things this way? Why not just tally up the costs of everything and give a final number? Because the final number will have erased all of the costs involved in production.

When we have a disaggregate list of costs for each product, producers, consumers, and governments can know the *exact* costs of something, as opposed to just *the market cost*. Fans of capitalism say that pricing allows you to compare the cost of two different things. They're right in that prices as aggregate values allow easy comparison, but they're also wrong because *prices don't reflect costs*. Prices are arbitrary and meaningless, whereas costs are absolute.

Basis allows tracking of *costs* of production, retaining information about the labor, resources, and to some extent the externalities it takes to make something.

## Public market

A company can publish the products and services they provide, and Basis lets other companies (or consumers) order them.

In a sense, Basis is like Amazon for companies: use it to order the things you need to run your company from other companies, and it will track the costs of production along the way.

For members ordering from other members, no money is involved in the transaction. This means that that the larger the network of member companies grows, the easier it is to produce.

## Banking

Because regions interface with the market system, they need some form of translation between credits earned from labor and the market currency. This is where the regional bank comes in. It acts as a holder for local currency and determines the exchange rate going from internal credits to external currency. The bank also enacts policies and criteria about the cost structure of companies. For instance, the bank determines the conditions under which a company is considered bankrupt (not providing enough value either to members or non-members to justify its existence).

## Credit system

In exchange for labor, credits are transferred to workers. How many credits per-hour or per-year is a decision made between the worker and the company, and is effectively the same as a wage in a market system.

When credits are spent (exchanged for products or services), they are destroyed. This way, they do not enter the productive economy but rather only serve as a means to track any member's contribution.

The *price* of products (how many credits are used to purchase items) is a function of not just how much labor went into creating that product, but the resources (such as raw materials) and the socially-decided credit value for each of those resources.

## Asset management

Let's say we communally own some houses, some office buildings, a few tractors...great! Who is in stewardship of them? Is there an end date to their use of that asset that we can use to determine if we need to acquire more? Are there specific terms for the use of it?

Basis allows us to track these assets, who has them, how old they are, the costs of their maintenance, and so on. This data is transparent to members so communal decisions can be made in confidence.

## Voting

What good is the communal ownership of assets if you cannot exercise any sort of control over them? Basis allows various types of elections so members of the system can have a say over their shared property.

This ranges from voting for people for specific permissions and positions in the system to voting for changing economic parameters.

The idea is to allow self-governance in many forms, and not to force a particular setup on people.

## Public companies/projects

At some point, it's conceivable that this system might grow to large numbers of members. At some point, these members might want to have public services and infrastructure. It makes sense to define the ability to create regional (or extra-regional) projects or services that have member-decided budgets or recurring costs.

Examples might be hospitals, schools, bridges, pharmaceutical research, space exploration, etc.

