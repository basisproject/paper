# Chapter 2: Membership

Membership in Basis is controlled by a few system-wide policies, and after that, by policies set by the members of a particular region. The global policy is fairly simple:membership can only be given to companies that are worker-owned or are multi-stakeholder companies with a significant portion owned by the workers, and member companies must use the Basis system for their selling, purchasing, and labor time tracking.

Workers of a member company are automatically members.

## Member services

Members have access to the assets owned by the region they are a member of, and this access is provided at-cost to them.

What does this mean? If a region owns an apartment building, members can use it freely provided they collectively cover the costs of property taxes and maintenance. In other words, the region will not engage in extracting profit from members. This applies to not just housing, but commercial property, large machinery, and anything else the region owns.

Members are given use of these assets, and while they are in use, are given control. So in our apartment example, if ten members share an apartment building, there is no landlord...those ten members *are* their own landlord and must set their own building rules, organize repairs, decide what portion of the expenses each of them pay (perhaps by number of bedrooms or square footage), and/or elect one of them to fulfill these duties. It's important to note that performing maintenance and repairs of these buildings *would constitute labor* and they would receive their share of credits for performing these tasks (which would be incorporated into the cost of the building rent).

## Key management

TODO

## Voting rights

Members have voting rights in the system proportional to their contribution in hours of labor. A person who works 30 hours a week has twice as much voting power as someone who works 15 hours a week. 

A cap may be set on the upper limit of hours needed to have full represenation, meaning that if the cap is set at 40 hours a week, someone who works 50 hours a week has as much voting rights as someone who works 40 hours a week. The purpose of this would be to encourage work-life balance and this cap is decided regionally.

## Member wallets

Members will have a "wallet" that stores their credits (more about this in chapter 6) which they can use to spend on goods and services in the Basis economy.

Both members and non-members can have any number of currency wallets which hold national currencies such as USD.

## Non-member users

Companies that are not members are free to use the system. They would be able to buy and sell products and services through the Basis network to both member and non-member companies. They may be charged a monthly fee for use of the service, and/or charged a per-transaction fee.

Non-member users may be allowed use of regionally-owned assets, however they will be charged at market-rate. For instance, if an apartment building has ten units, five might be filled by members (who would be the property managers) and the other five might be rented out at market rate (the difference in the at-cost rent and the market rate going to the region's bank).

This concept of duality not only creates incentives for membership (at-cost use of regional assets) but also allows for growth of the region's assets: new houses, apartments, office buildings, etc. Members are provided everything at-cost, non-members are provided at profit-extraction rates, with the difference going back to the region.

## Loss of membership

As membership can given based on global and regional policy, it can also be revoked. For instance, a worker co-op that converts to a privately-owned corporation would lose membership in the system.

Instead of forfeiting use of all regionally-owned assets, the region would simply engage in profit-extraction. For instance, if the company uses an office building, it is no longer provided at-cost, but rather at market-rate, and same for any housing used by the company members: they would have to start paying rent at market rate.


