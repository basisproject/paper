# Chapter 3: Regions

Regions are a core concept of the system. They define a geographical area (such as a city or county) that owns a set of assets which are managed by the members of that region. A region might own apartment buildings, houses, factories, warehouses, office buildings, tractors, and any other large assets related to production or housing.

The goal of regions is to give the members of a particular area the ability to self-manage their own means of existence.

## Asset ownership

All communal assets are assigned to a particular region, and the members of that region have ultimate control over it. They can sell it on the market and use the funds to buy another asset. They can rent it out on the open market. They can demolish it completely. Asset ownership is regional.

## Regional parameters

Regions don't just own things. They also have a set of member-decided parameters such as:

- Number of average weekly hours worked required to be considered a member
- Number of average weekly hours worked above which someone is considered a full member
- Worker credit wage cap
- Distribution plans for regional profits
- Budgets for regional projects and services (hospitals, schools, etc)

## Governance

Regions are self-governing, and therefor are free to choose whatever structure they see fit. For instance, how is it decided to buy a new asset? If there is a regionally-owned bank, what is its investment strategy? Are these decided via direct election or does the region elect a minister or council to handle these decisions?

Governance in the Basis system is done using a system of permissions that apply either to a member or a group of members (a council). Permissions can be region-wide or only pertain to a specific class of regional assets. No matter the permissions given or how they apply, permissions are given by members and can be revoked by members.

Basis gives members the tools to create their own governance structures and make these decisions without imposing any pre-determined blueprint. Regions are free to self-govern as they see fit.


