# Chapter 7: Credit system

Workers, in exchange for their labor, receive credits. After a labor record has been finalized by the company, credits are added to that worker's Basis account balance.

Credits can be spent on goods and services provided by companies in the Basis system, and are destroyed on spending. Credits do not expire, and are transferable to other members freely.

## Labor and resources

In exchange for labor that meets social needs, workers get paid in credits. The amount they get paid or the particular arrangement they set up is between them and the company they are working with. Wages are completely negotiable (although a minimum/maximum wage can be set by each region if desired), and can be based off of hourly work or some form of ongoing salary.

Credits are used to make purchases by members. How many credits something costs is a function of.

1. How much labor went into producing it.
1. How much resources were used in its production.

The cost of various resources will be decided both *regionally* and *systemwide* via democratic process. Participants are able to set costs on resources globally, and in addition to global costs, can set additional regional costs on resources. This allows implementing things like carbon taxes, but also local rations on certain local resources (for instance, water might be rationed in a region if it experiences a drought). The credit value for each resource might be based on resource consumption rates, renewal rates, and known supply.

So if a chair took 5 hours to make, and is comprised of 3kg wood, and wood costs 0.4 credits/kg, then the total cost of the chair would be `5 + (3 * 0.4) = 6.2` credits.

Thus, the cost of a final product can fluctuate somewhat depending on what the resource content of that product is. This allows fairly direct and simple implementations of things like carbon taxes or rations on certain resources in low supply.

## Currency exchange

Exchange between credits and national currency will be possible through the regional bank. This bank will have a pool of capital specifically used to back the value of the credits. Members can convert their credits into currency (say, USD), however credit exchange is one-way: *credits will never be issued in exchange for currency*. The only way for new credits to be issued is to perform labor in the system! This prevents speculation on the market value of labor credits and protects members from sudden changes in value caused by external forces.

Currency exchange happens at a rate of `total regional credit fund / total active regional credits`. If the region has $1M in funds and has 100K active credits, then each credit exchanges for exactly $10. It's important to note that the credit fund can be managed such that the amount of funds closely or exactly matches the number of active credits, creating a 1:1 peg on the local currency. This is up to the region and its banking policies to decide if this is desired.

